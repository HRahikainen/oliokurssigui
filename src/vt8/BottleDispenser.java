/**
 * BottleDispenser.java
 * 13.9.2017
 */
package vt8;

import java.text.DecimalFormat;
/**
 * @author Harri
 *
 */
import java.util.ArrayList;

public class BottleDispenser {
    
	private static BottleDispenser instance = null;
    private int bottles;
    private static ArrayList<Bottle> bottle_array;
    private double money;
    
    private BottleDispenser() {
        bottles = 6;
        money = 0;
        
        bottle_array = new ArrayList<Bottle>();
        
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.5, 1.8));
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Fanta", 0.5, 1.95));
    }
    
    public static BottleDispenser getInstance() {
    	if(instance == null) {instance = new BottleDispenser();}
    	else {System.out.println("You can see the dispenser right there already!");}
    	return instance;
    }
    
    public String addMoney(double amount) {
        money += amount;
        return "Klink! Lis�� rahaa laitteeseen!";
    }
    // TODO: Change this to work with comboBox choice
    public String buyBottle(Bottle b) {
    	String msg = "";
    	if (money < b.getPrice()) {
    		msg = "Sy�t� rahaa ensin!";
    	}else if (bottles <= 0) {
    		msg = "Pullot meni negatiiviseksi, t�yt�!";
        	bottles = 0;
    	}else {
    		money -= b.getPrice();
    		msg = "KACHUNK! " + b.getName() + " tipahti masiinasta!";
    		bottles -= 1;
    		deleteBottle(b);
    	}
    	return msg;

    }
    
    public String returnMoney() {
    	DecimalFormat df = new DecimalFormat("#0.00");
    	String moneyString = "";
    	moneyString = df.format(money);
    	money = 0;
        return "Klink klink. Sinne meniv�t rahat! Rahaa tuli ulos " + moneyString.replace(".", ",")+ "�";
    }
    
    public ArrayList<Bottle> getBottleArray(){
    	return bottle_array;
    }
    
    private void deleteBottle(Bottle b) {
        bottle_array.remove(b);
    }
    
    public double getMoney() {
    	return money;
    }

}

