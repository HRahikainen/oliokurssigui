package vt8;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Slider;
import javafx.scene.control.ComboBox;

public class PulloController implements Initializable{

    @FXML private Label title;
    @FXML private Button moneyBtn;
    @FXML private Button buyBtn;
    @FXML private TextArea machineLogArea;
    @FXML private Button cashBackButton;
    @FXML private Button receiptBtn;
    @FXML private Slider moneySlider;
    @FXML private Label moneyLabel;
    @FXML private ComboBox<String> brandMenu;
    @FXML private Label moneyLeftLabel;
    
    private double selectedMoney;
    private BottleDispenser pullomaatti = BottleDispenser.getInstance();
    private ArrayList<String> boughtBottles = new ArrayList<String>();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        populateBoxes(pullomaatti.getBottleArray());
    }
    
    public void populateBoxes(ArrayList<Bottle> bottle_arr) {
        for(Bottle bottle : bottle_arr) {
        	if(!brandMenu.getItems().contains(bottle.getInfo()))
        		brandMenu.getItems().add(bottle.getInfo());
        }
    }
    
    public String getMoneyLeft() {
    	Double totMoney = new Double(pullomaatti.getMoney());
    	totMoney = (double)Math.round(totMoney * 100) / 100;
    	
    	DecimalFormat df = new DecimalFormat("#0.00");
    	String moneyString = df.format(totMoney);
    	return moneyString;
    }

    @FXML
    void getSliderValue(MouseEvent event) {
    	Double dmoney = new Double(moneySlider.getValue());
    	dmoney = (double)Math.round(dmoney * 100) / 100;
    	selectedMoney = dmoney;
    	
    	DecimalFormat df = new DecimalFormat("#0.00");
    	String moneyString = df.format(moneySlider.getValue());
    	moneyLabel.setText(moneyString);
    }

    @FXML
    void cashBack(ActionEvent event) {
    	machineLogArea.setText(pullomaatti.returnMoney());
    	moneyLeftLabel.setText(getMoneyLeft());
    }
   
    @FXML
    void addMoneyAction(ActionEvent event) {
    	pullomaatti.addMoney(selectedMoney);
    	moneySlider.adjustValue(0);
    	selectedMoney = 0;
    	moneyLabel.setText("0.00");
    	moneyLeftLabel.setText(getMoneyLeft());
    }

    @FXML
    void buyBottleAction(ActionEvent event) {
    	//brandMenu.getSelectionModel().getSelectedIndex();
    	String chosenBottle = brandMenu.getValue();
    	for(Bottle b : pullomaatti.getBottleArray()) {
    		String idString = b.getInfo();
    		if(chosenBottle.contentEquals(idString)){
    			machineLogArea.setText(pullomaatti.buyBottle(b));
    			boughtBottles.add(idString);
    			break;
    		}else {
    			machineLogArea.setText("Valitsemaanne pulloon ei juuri nyt saada yhteytt�...");
    		}
    	}
    	moneyLeftLabel.setText(getMoneyLeft());
    }

    @FXML
    void writeReceiptAction(ActionEvent event) {
    	String filename = "./receipt.txt";
    	try {
    		
			BufferedWriter out = new BufferedWriter(new FileWriter(filename)); 
			out.write("###KUITTI###\n");
			for(String purchase : boughtBottles)
				out.write(purchase + "\n"); 
			out.close();
			
			
		} catch (IOException ioe) {
			machineLogArea.appendText("\nReceipt could not be written!");
		}
    }

}
