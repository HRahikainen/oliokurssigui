/**
 * Bottle.java
 * 13.9.2017
 */
package vt8;

/**
 * @author Harri
 *
 */
public class Bottle {
	
	private String name;
	private String manufacturer;
	private double total_energy;
	private double price;
	
	public Bottle() {
		this.name = "Pepsi Max";
		this.manufacturer = "Pepsi";
		this.total_energy = 0.5;
		this.price = 1.80;
	}
	
	public Bottle(String name, String manuf, double totE, double price) {
		this.name = name;
		this.manufacturer = manuf;
		this.total_energy = totE;
		this.price = price;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getManufacturer() {
		return this.manufacturer;
	}
	
	public double getEnergy() {
		return this.total_energy;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public String getInfo() {
		return getName() + " " + Double.toString(getEnergy()) + "l " + Double.toString(getPrice()) + "�";
	}
}
