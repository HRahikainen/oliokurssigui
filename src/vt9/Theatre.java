package vt9;

import java.util.ArrayList;

public class Theatre {
	private String id;
	private String name;
	private ArrayList<Show> showList = new ArrayList<Show>();
	
	public Theatre(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String getId() {
		return id;
	}
	
	public ArrayList<Show> getShows(){
		return showList;
	}
	public void setShows(ArrayList<Show> s){
		this.showList = s;
	}
	
}
