package vt9;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
/**
 * @author Harri
 *
 */
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class KinoController implements Initializable{

    @FXML private Label title;
    @FXML private Label selectTheaterLabel;
    @FXML private ComboBox<String> selectTheaterList;
    @FXML private Label selectStartTimeLabel;
    @FXML private Button listMoviesBtn;
    @FXML private TextField showDateField;
    @FXML private TextField showStartsField;
    @FXML private TextField showEndsField;
    @FXML private Label searchLabel;
    @FXML private TextField srcMovieByNameField;
    @FXML private Button srcMovieByNameBtn;
    @FXML private ListView<String> showInfoList;

    private TheatreManager manager = TheatreManager.getInstance();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	// Fill ComboBox
    	for(Theatre t : manager.getList()) {
    		selectTheaterList.getItems().add(t.getName());
    	}
    }
    
    @FXML void listMoviesAction(ActionEvent event) {
    	// Get selected index, date and start and end times
    	try {
    		int index = selectTheaterList.getSelectionModel().getSelectedIndex();
    		Theatre t = manager.getList().get(index);
    		String id = t.getId();
    		String dateString = getDate();
    		String startString = getStartTime();
    		String endString = getEndTime();
    		t.setShows(ConnectAndParse.parseShowData(id, dateString, startString, endString));
    		showInfoList.getItems().clear();
    		
    		// Fill ListView
    		for(Show s : t.getShows()) {
    			if(!showInfoList.getItems().contains(t.getName() + ": " + s.toString())){
    				showInfoList.getItems().add(t.getName() + ": " + s.toString());
    			}
        	}
    		
    	} catch(ArrayIndexOutOfBoundsException e) {
    		showInfoList.getItems().add("Choose a theatre first!");
    	}
    }

    @FXML void srcMovieByNameAction(ActionEvent event) {
    	String movieName = srcMovieByNameField.getText();
    	showInfoList.getItems().clear();
    	showInfoList.getItems().add("Esitykset elokuvalle: " + movieName);
    	
    	for(Theatre t : manager.getList()) {
    		t.setShows(ConnectAndParse.parseShowData(t.getId(), "", "", ""));
    		for(Show s : t.getShows()) {
    			if(s.getTitle().equals(movieName)) {
    				showInfoList.getItems().add(t.getName() + ": " + s.toString());
    			}
    		}
    	}
    }
    
    private String getDate() {
    	return showDateField.getText();
    }
    
    private String getStartTime() {
    	return showStartsField.getText();
    }
    
    private String getEndTime() {
    	return showEndsField.getText();
    }
    
}
