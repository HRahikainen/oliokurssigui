package vt9;

import java.util.ArrayList;

public class TheatreManager {
	private static TheatreManager instance = null;
	private ArrayList<Theatre> theatreList = new ArrayList<Theatre>();
	
	public static TheatreManager getInstance() {
		if(instance == null) {
			instance = new TheatreManager();
		} 
		return instance;
	}
	private TheatreManager() {
		theatreList = ConnectAndParse.parseTheatreData();
	}
	
	public ArrayList<Theatre> getList() {
		return theatreList;
	}
	
	

}
