package vt9;

public class Show {
	private String title;
	private String genre;
	private String start;
	private String end;
	
	public Show(String title, String genre, String start, String end) {
		this.title = title;
		this.genre = genre;
		this.start = start;
		this.end = end;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public String getStart() {
		return start;
	}

	public String getEnd() {
		return end;
	}

	@Override
	public String toString() {
		return title + " (" + genre + ")" +  start + " - " + end;
	}
}
