package vt9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ConnectAndParse {
	private static Document doc;
	private static ArrayList<Theatre> tList = new ArrayList<Theatre>();
	private static ArrayList<Show> sList = new ArrayList<Show>();
	
	private static String gatherData() throws Exception {
        URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");
        return readURLToString(url); 
    }
	
	private static String gatherData(String id, String date, String startTime, String endTime) throws Exception{
		String queryString = makeQueryString(id, date);
		URL url = new URL("http://www.finnkino.fi/xml/Schedule/?" + queryString);
		return readURLToString(url);
	}
	
	public static ArrayList<Theatre> parseTheatreData() {
		try {
			String dataString = gatherData();
			docPreparator(dataString);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        getTheatreData();
		return tList;
	}
	
	public static ArrayList<Show> parseShowData(String id, String date, String startTime, String endTime) {
		try {
			sList.clear();
			date = formatDateTime(date);
			String dataString = gatherData(id, date, startTime, endTime);
			docPreparator(dataString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		getShowData();
		ArrayList<Show> filteredShows = new ArrayList<Show>();
		Date parsedDate = null;
		
		try {
			parsedDate = new SimpleDateFormat("dd.MM.yyyy").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(startTime.equals("")) {startTime = "00:00";}
		if(endTime.equals("")) {endTime = "23:59";}
		
		startTime = new SimpleDateFormat("yyyy-MM-dd").format(parsedDate) + "T" + startTime + ":00";
		endTime = new SimpleDateFormat("yyyy-MM-dd").format(parsedDate) + "T" + endTime + ":00";
		
		
		
		for(Show s : sList) {
			LocalDateTime startdttm = LocalDateTime.parse(s.getStart(),DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			LocalDateTime enddttm = LocalDateTime.parse(s.getEnd(),DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			LocalDateTime startAlias = LocalDateTime.parse(startTime,DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			LocalDateTime endAlias = LocalDateTime.parse(endTime,DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			if((startdttm.isAfter(startAlias) && enddttm.isBefore(endAlias))) {
				filteredShows.add(s);
			}
		}
		return filteredShows;
	}
	
    private static void getTheatreData() {
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            Theatre t = new Theatre(getTagText("ID", e), 
            		getTagText("Name", e));

            if(!getTagText("Name", e).contains("Valitse alue/teatteri")) {
            	tList.add(t);
            }
        }
    }
    
    private static void getShowData() {
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            Show s = new Show(getTagText("Title", e), getTagText("Genres", e), getTagText("dttmShowStart", e), getTagText("dttmShowEnd", e));
            sList.add(s);
        }
    }
    
    private static String getTagText(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
    
    private static void docPreparator(String dataString) {
    	try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(dataString)));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
        }
    }
    
    private static String readURLToString(URL url) throws Exception{
    	BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String inputLine, total="";
        
        while((inputLine = in.readLine()) != null) {
            total += inputLine+"\n";
        }
        in.close();
        return total;
    }
    
   private static String formatDateTime(String input) {
    	String dt = input;
    	if(input.equals("")) { 
    		dt = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
    	}
   		return dt;
   }
    
   private static String makeQueryString(String id, String date) {
	   String query = "area=" + id + "&dt=" + date;
	   return query;
   }

}
