package vt10;

import java.net.URL;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebView;
import netscape.javascript.JSException;

public class WebController implements Initializable{

	@FXML private Button previousBtn;
	@FXML private Button nextBtn;
	@FXML private Button refreshBtn;
	@FXML private Button homeBtn;
	@FXML private TextField urlField;
	@FXML private WebView webWindow;
	
	private ArrayList<String> history = new ArrayList<String>();
	//private String nextAddr = "";
	//private String prevAddr = "";
	private int currentIdx = 0;
	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
        webWindow.getEngine().load(getClass().getResource("index2.html").toExternalForm());
        history.add(webWindow.getEngine().getLocation());
        currentIdx = history.size();
        nextBtn.setDisable(true);
        previousBtn.setDisable(true);
    }

	@FXML void onEnterPressed(KeyEvent ke) {
		currentIdx = history.size();
    	if(ke.getCode().equals(KeyCode.ENTER)) {
    		String url = checkURL();
    		if(!url.equals(webWindow.getEngine().getLocation())) {
	    		previousBtn.setDisable(false);
	    		if(!(history.get(history.size() - 1).equals(url))) {
	    			for(int x = history.size() - 1; x > currentIdx; x--)
	    			{
	    			    history.remove(x);
	    			}
	    			history.add(url);
	    			currentIdx = history.size() - 1;
	    		}
	    		if(history.size() > 10) {
	    			history.remove(0);
	    		}
	    		/*nextAddr = url;
	    		prevAddr = webWindow.getEngine().getLocation();*/
    		}
    		webWindow.getEngine().load(url);
    		urlField.setText(webWindow.getEngine().getLocation());
    	}
    }
	
	@FXML void loadHomePage(ActionEvent event) {
		//prevAddr = webWindow.getEngine().getLocation();
		// Still needs some fixing
		nextBtn.setDisable(true);
		if(!(history.get(history.size() - 1).equals(webWindow.getEngine().getLocation()))) {
			for(int x = history.size() - 1; x > currentIdx; x--)
			{
			    history.remove(x);
			}
			history.add(webWindow.getEngine().getLocation());
			currentIdx = history.size() - 1;
		}
		
		if(history.size() > 10) {
			history.remove(0);
		}
		webWindow.getEngine().load(getClass().getResource("index2.html").toExternalForm());
		urlField.setText(webWindow.getEngine().getLocation());
    }
	
	@FXML void getPreviousPage(ActionEvent event) {
		/*if(!prevAddr.equals("") && !prevAddr.equals(webWindow.getEngine().getLocation())) {
			nextBtn.setDisable(false);
			nextAddr = webWindow.getEngine().getLocation();
			webWindow.getEngine().load(prevAddr);	
		}*/
		if(history.listIterator(currentIdx).hasPrevious()) {
			nextBtn.setDisable(false);
			webWindow.getEngine().load(history.listIterator(currentIdx).previous());
			currentIdx--;
			if(currentIdx == 0) {
				previousBtn.setDisable(true);
				currentIdx++;
			}
		}else {
			previousBtn.setDisable(true);
		}
		urlField.setText(webWindow.getEngine().getLocation());
	}

	@FXML void getNextPage(ActionEvent event) {
		/*if(!nextAddr.equals("") && !nextAddr.equals(webWindow.getEngine().getLocation())) {
			previousBtn.setDisable(false);
			prevAddr = webWindow.getEngine().getLocation();
			webWindow.getEngine().load(nextAddr);
		}*/
		if(history.listIterator(currentIdx).hasNext()) {
			previousBtn.setDisable(false);
			webWindow.getEngine().load(history.listIterator(currentIdx).next());
			currentIdx++;
			if(currentIdx == (history.size())) {
				nextBtn.setDisable(true);
				currentIdx--;
			}
		}else {
			nextBtn.setDisable(true);
		}
		urlField.setText(webWindow.getEngine().getLocation());
	}

	@FXML void refreshPage(ActionEvent event) {
		webWindow.getEngine().reload();
		urlField.setText(webWindow.getEngine().getLocation());
    }
	
	@FXML public void shoutBtnClicked(ActionEvent event) {
		try {
			webWindow.getEngine().executeScript("document.shoutOut()");
		}catch(JSException ex) {};
	}
	
	@FXML void initializeBtnClicked(ActionEvent event) {
		try {
	        webWindow.getEngine().executeScript("initialize()");
		}catch(JSException ex) {};
	}
	
	private String checkURL() {
		String url = urlField.getText();
		if(!url.contains("http://") && !url.contains("https://")) {
			url = "http://" + url;
		}
		return url;
	}
}
