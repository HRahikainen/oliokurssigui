package vt11;

import java.util.HashMap;

public class ShapeHandler {

	private static ShapeHandler instance = null;
	private HashMap<String, Point> pointMap = new HashMap<String, Point>();
	private HashMap<String, Viiva> lineMap = new HashMap<String, Viiva>();
	
	private ShapeHandler() {}
	
	public static ShapeHandler getInstance() {
		if(instance == null) {
			instance = new ShapeHandler(); 
		}
		return instance;
	}
	
	public HashMap<String, Point> getPointMap(){
		return pointMap;
	}

	public HashMap<String, Viiva> getLineMap(){
		return lineMap;
	}
}
