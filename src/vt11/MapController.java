package vt11;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

public class MapController implements Initializable{
	
    @FXML private AnchorPane base;
    private ArrayList<Point> pList = new ArrayList<Point>();
    
	public void initialize(URL url, ResourceBundle rb) {
		
	}

    @FXML void baseMouseClicked(MouseEvent event) {
    	HashMap<String, Point> pmap = ShapeHandler.getInstance().getPointMap();
    	HashMap<String, Viiva> lmap = ShapeHandler.getInstance().getLineMap();
    	// Check if click event on existing point's circle
    	for(Point i: pmap.values()) {
    		Circle c = i.getCircle();
    		if(isInsideCircle(c, event)) {
    			// If inside circle and start point selected, add and draw line, finally delete start point
    			// to avoid all lines starting from one point
    			if(!pList.isEmpty()) {
    				Viiva v = new Viiva(pList.get(0), i);
    				lmap.put("Viiva" + Integer.toString(lmap.size()), v);
    				base.getChildren().add(v.getLine());
    				pList.clear();
    			}else {
    				pList.add(i);
    			}
    			return;
    		}
    	}
    	// If not in circle, add new one
    	Point p = new Point(event.getX(), event.getY());
	    base.getChildren().add(p.getCircle());
        pmap.put("Pointti" + Integer.toString(pmap.size()) , p);
    }
    // Math happens
    private Boolean isInsideCircle(Circle c, MouseEvent event) {
    	if(Math.sqrt(Math.pow(event.getSceneX() - c.getLayoutX(), 2) + Math.pow(event.getSceneY() - c.getLayoutY(), 2)) <= c.getRadius()) {
    		return true;
    	}
    	return false;
    }

}
