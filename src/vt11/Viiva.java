package vt11;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Viiva {
	private String name;
	private Point start;
	private Point end;
	private Line l;
	
	public Viiva(Point start, Point end) {
		this.start = start;
		this.end = end;
		this.l = new Line(start.getCircle().getLayoutX(),start.getCircle().getLayoutY(), end.getCircle().getLayoutX(), end.getCircle().getLayoutY());
		l.setStroke(Color.RED);
		l.setStrokeWidth(2);
	}

	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Line getLine() {
		return this.l;
	}

}
