package vt11;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Point {
	private String name;
	private Circle c;
	
	public Point(double x, double y) {
		this.c = new Circle();
		c.setLayoutX(x);
        c.setLayoutY(y);
        c.setRadius(10);
        c.setFill(Color.BLUE);
        c.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                System.out.println("Hei, olen piste!" + " " + event.getSceneX()  + " " + event.getSceneY());
            }});
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Circle getCircle() {
		return this.c;
	}
}
