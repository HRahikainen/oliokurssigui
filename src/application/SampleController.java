package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class SampleController {

    @FXML
    private Label label;
    
    @FXML
    private TextField textField;
    
    @FXML
    private Button saveBtn;
    
    @FXML
    private TextField savePath;

    @FXML
    private Button loadBtn;
    
    @FXML
    private TextField loadPath;

    @FXML
    private TextArea editorTextArea;

    @FXML
    void handleButtonAction(ActionEvent event) {
    	//System.out.println("Hello World!");
    	//label.setText("Hello World!");
    	label.setText(textField.getText());
    	textField.clear();
    }
    
    @FXML
    void onEnter(KeyEvent ke) {
    	if(ke.getCode().equals(KeyCode.ENTER)) {
    		label.setText(textField.getText());
        	textField.clear();
    	}
    }
    
    @FXML
    void loadText(MouseEvent event) {
    	String output = "";
		
		try {
			BufferedReader in; 
			String inputLine;
			String filename = loadPath.getText();
			
			in = new BufferedReader(new FileReader(filename));
			while((inputLine = in.readLine()) != null) {
    			output += inputLine + "\n";
			}
			in.close();
			editorTextArea.setText(output);
			
		} catch (IOException ioe) {
			//editorTextArea.setText("File not found!");
			editorTextArea.appendText("\nFile not found!");
		}
		
    }


    @FXML
    void saveText(MouseEvent event) {
		
		try {
			String filename = savePath.getText();
			BufferedWriter out = new BufferedWriter(new FileWriter(filename)); 
			String textToSave = editorTextArea.getText();
			out.write(textToSave); 
			out.close();
			
			
		} catch (IOException ioe) {
			//editorTextArea.setText("File could not be written!");
			editorTextArea.appendText("\nFile could not be written!");
		}
    }

}
